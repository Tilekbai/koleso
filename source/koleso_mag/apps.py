from django.apps import AppConfig


class KolesoMagConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'koleso_mag'
