from django.shortcuts import render

# Create your views here.

def index_view(request):
    return render (request, 'index.html')

def contacts_view(request):
    return render (request, 'contacts.html')

def aboutus_view(request):
    return render (request, 'about_us.html')

def for_clients_view(request):
    return render (request, 'for_clients.html')

def calculator_view(request):
    if request.method == 'GET':
        return render(request, 'calculator.html')
    elif request.method == 'POST':
        if request.POST.get('operation')=='PLUS':
            context = {
                '1num': request.POST.get('1num'),
                '2num': request.POST.get('2num'),
                'operation': request.POST.get('operation'),
                'res': float(request.POST.get('1num')) + float(request.POST.get('2num'))
            }
        
        if request.POST.get('operation') == 'MINUS':
            context = {
                '1num': request.POST.get('1num'),
                '2num': request.POST.get('2num'),
                'operation': request.POST.get('operation'),
                'res': float(request.POST.get('1num')) - float(request.POST.get('2num'))
            }
        
        if request.POST.get('operation') == 'MULTIPLY':
            context = {
                '1num': request.POST.get('1num'),
                '2num': request.POST.get('2num'),
                'operation': request.POST.get('operation'),
                'res': float(request.POST.get('1num')) * float(request.POST.get('2num'))
            }
        
        if request.POST.get('operation') == 'DIVIDE':
            context = {
                '1num': request.POST.get('1num'),
                '2num': request.POST.get('2num'),
                'operation': request.POST.get('operation'),
                'res': float(request.POST.get('1num')) / float(request.POST.get('2num'))
            }
        return render(request, 'calc_result_view.html', context)

